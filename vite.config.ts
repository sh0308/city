import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path';
import { svgBuilder } from "./src/plugins/svgBuilder"



// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    // 注册所有的svg文件生成svg雪碧图
    svgBuilder("./src/assets/icons/")
  ],
  base: './',
  resolve: {
    // https://cn.vitejs.dev/config/#resolve-alias
    // https://cn.vitejs.dev/config/#resolve-extensions
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
    // 配置路径别名
    alias: {
      '@': resolve(__dirname, 'src'),
    },
  },
  server: {
    port: 10001,
    host: '0.0.0.0'
  }
})
