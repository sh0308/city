/// <reference types="vite/client" />

declare module '*.vue' {
    import { DefineComponent } from 'vue'
    // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
    const component: DefineComponent<{}, {}, any>
    export default component
}
declare module "chinese-lunar-calendar"
declare module "@amap/amap-jsapi-loader"
declare module "nprogress"
declare module "*.json" {
    const jsonValue: any;
    export default jsonValue;
}