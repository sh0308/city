import { http } from "@/utils/request";

export function appImagesSelectAll(param: any) {
    return http.get("/appImages/selectAll", param);
}
export function appImagesSelectOne(param: number) {
    return http.get("/appImages/" + param);
}

export function appImagesInsert(project: any) {
    return http.post("/appImages/insert", project);
}
export function appImagesDelete(ids: any) {
    return http.post("/appImages/delete", undefined, { idList: ids });
}

export function appImagesUpdate(myParam: any) {
    return http.post("/appImages/update", myParam);
}

