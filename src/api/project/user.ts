import { http } from "@/utils/request";

export function nurseUserSelectAll(param: any) {
    return http.get("/nurseUser/selectAll", param);
}

export function nurseUserInsert(project: any) {
    return http.post("/nurseUser/insert", project);
}
export function nurseUserDelete(ids: any) {
    return http.post("/nurseUser/delete", undefined, { idList: ids });
}

export function nurseUserUpdate(param: any) {
    return http.post("/nurseUser/update", param);
}

// 获取客户端所有用户收益情况
export function nurseUserGetUserEarnings(param: any) {
    return http.get("/nurseUser/opertGetUserEarnings", param);
}
