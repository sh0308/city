import { http } from "@/utils/request";

export function appNewsSelectAll(param: any) {
    return http.get("/appNews/selectAll", param);
}

export function appNewsInsert(project: any) {
    return http.post("/appNews/insert", project);
}
export function appNewsDelete(ids: any) {
    return http.post("/appNews/delete", undefined, { idList: ids });
}

export function appNewsUpdate(myParam: any) {
    return http.post("/appNews/update", myParam);
}

export function appNewsSelectOne(param: number) {
    return http.get("/appNews/" + param);
}


