import { http } from "@/utils/request";

export function appProtocolSelectAll(param: any) {
    return http.get("/appProtocol/selectAll", param);
}
export function appProtocolSelectOne(param: number) {
    return http.get("/appProtocol/" + param);
}

export function appProtocolInsert(project: any) {
    return http.post("/appProtocol/insert", project);
}
export function appProtocolDelete(ids: any) {
    return http.post("/appProtocol/delete", undefined, { idList: ids });
}

export function appProtocolUpdate(myParam: any) {
    return http.post("/appProtocol/update", myParam);
}

