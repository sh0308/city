import { http } from "@/utils/request";

export function selectNurseUserList(param: any) {
    return http.get("/nurseUser/selectNurseUserList", param)
}

// 通过租户id修改数据
export function nurseUserUpdate(param: any) {
    return http.post("/nurseUser/update", param)
}

// 通过租户id删除数据
export function nurseUserDelete(param: any) {
    return http.post("/nurseUser/delete", param)
}