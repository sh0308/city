import { http } from "@/utils/request";

// 获取运营中心id所有设备
export function nurseDeviceSelectAll(param: any) {
    return http.get("/nurseDevice/quertyDeviceListByUserId", param);
}

// 添加一台设备
export function nurseDeviceInsert(project: any) {
    return http.post("/nurseDevice/insert", project);
}

export function addNurseDevice(deviceNo: any, deviceName: any, serviceUserId: any) {
    return http.post("/nurseDevice/addNurseDevice", { deviceNo: deviceNo, deviceName: deviceName, version: serviceUserId });
}
export function nurseDeviceDelete(ids: any) {
    return http.post("/nurseDevice/delete", undefined, { idList: ids });
}

// 修改设备信息
export function nurseDeviceUpdate(myParam: any) {
    return http.post("/nurseDevice/update", myParam);
}

// 将指定设备进行锁机
export function nurseDeviceSendLock(myParam: any) {
    return http.post("/nurseDevice/sendLock", myParam);
}

// 给指定设备进行解锁
export function nurseDeviceSendUnlock(myParam: any) {
    return http.post("/nurseDevice/sendUnlock", myParam);
}

export function nurseDeviceUpdateByuserId(param: any) {
    return http.post("/nurseDevice/nurseDeviceUpdateByuserId", param);
}

export function deviceTaskSelectAll(param: any) {
    return http.get("/deviceTask/selectAll", param);
}

export function deviceTaskInsert(project: any) {
    return http.post("/deviceTask/insert", project);
}
export function deviceTaskDelete(ids: any) {
    return http.post("/deviceTask/delete", undefined, { idList: ids });
}

export function deviceTaskUpdate(myParam: any) {
    return http.post("/deviceTask/update", myParam);
}

// 申请设备
export function nurseDeviceApplyDevice(params: any) {
    return http.post("/nurseDevice/applyDevice", params);
}

// 设备划拨
export function deviceAllot(myParam: any) {
    return http.post("/nurseDevice/deviceAllot", myParam);
}

// 申请设备记录列表
export function applyDeviceList(param: any) {
    return http.get("/nurseDevice/applyDeviceList", param)
}

// 获取所有运营中心
export function sysUserSelectAllOpentName(param: any) {
    return http.get("/sysUser/selectAllOpentName", param)
}

// 通过运营中心获取下级的所有服务站点
export function sysUserSelectserviceUserById(param: any) {
    return http.get("/sysUser/selectserviceUserById", param)
}

// 获取我的设备申请记录列表
export function nurseDeviceSelectApplyById(param: any) {
    return http.get("/nurseDevice/selectApplyById", param)
}

// 设备调度完成签收响应
export function nurseDeviceUpdateApplyStatus(param: any) {
    return http.post("/nurseDevice/updateApplyStatus", param)
}

// 手动退租申请
export function cancellationApplication(param: any) {
    return http.post("/nurseOpert/cancellationApplication", param)
}