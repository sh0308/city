import { http } from "@/utils/request";

// 仓库设备
export function getDeviceWarehouseRecord(param: any) {
    return http.post("/DeviceWarehouse/getDeviceWarehouseRecord", param);
}

export function addDeviceWarehouseRecord(project: any) {
    return http.post("/DeviceWarehouse/addDeviceWarehouseRecord", project);
}
