import { http } from "@/utils/request";

export function searchHotSelectAll(param: any) {
    return http.get("/searchHot/selectAll", param);
}

export function searchHotInsert(project: any) {
    return http.post("/searchHot/insert", project);
}
export function searchHotDelete(ids: any) {
    return http.post("/searchHot/delete", undefined, { idList: ids });
}

export function searchHotUpdate(myParam: any) {
    return http.post("/searchHot/update", myParam);
}

export function searchHotSelectOne(param: number) {
    return http.get("/searchHot/" + param);
}