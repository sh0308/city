import { http } from "@/utils/request";

// 获取所有设备租赁记录
export function nurseDeviceLeaseRecord(param: any) {
    return http.get("/nurseDevice/leaseRecord", param);
}

// 获取所有主动退租申请记录
export function getExitApplyListOfOperatId(param: any) {
    return http.get("/exitApply/getExitApplyListOfOperatId", param);
}

// 获取所有（关于自己）主动退租申请记录
export function getExitApplyListOfOperatOneself(param: any) {
    return http.get("/exitApply/getExitApplyListOfOperatOneself", param);
}

// 审批-设备可调度明细列表
export function getTerminationList(param: any) {
    return http.get("/exitApply/getTerminationList", param)
}

// 审批-回收设备
export function goBack(param: any) {
    return http.post("/termination/goBack", param)
}

// 审批-调拨设备
export function transfer(param: any) {
    return http.post("/termination/transfer", param)
}

// 审批-寄出邮递
export function postMail(param: any) {
    return http.post("/termination/mail", param)
}

// 获取所有可签收货物
export function getSign(param: any) {
    return http.get("/termination/getSign", param)
}

// 审批-签收
export function sign(param: any) {
    return http.post("/termination/sign", param)
}

// 审批-确认收货
export function signOfOperat(param: any) {
    return http.post("/termination/signOfOperat", param)
}
