import { http } from "@/utils/request";

// 查询公司all收益
export function selectAllEarnings(param: any) {
    return http.get("/earnings/selectAllEarnings", param)
}

// 查询服务站点的收益情况
export function selectServerEarnings(param: any) {
    return http.get("/earnings/selectServerEarnings", param)
}

// 获取所有用户收益
export function getUserEarnings(param: any) {
    return http.get("/nurseUser/opertGetUserEarnings", param)
}

// 收益表头汇总数据
export function getSummarizing(param: any) {
    return http.get("/earnings/getSummarizing", param)
}