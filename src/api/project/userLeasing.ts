import { http } from "@/utils/request";

// 获取所有租赁设备的用户
export function selectNurseUserLeaseList(param: any) {
    return http.get("/nurseUser/selectNurseUserLeaseList", param);
}