import { http } from "@/utils/request";

// 用户查看报修工单
export function checkRepairById(param: any) {
    return http.post("/Maintenance/checkRepairById", param);
}

// 用户查看报修工单
export function assign(param: any) {
    return http.post("/Maintenance/assign", param);
}

// 维修人员确认工单
export function confirmOrder(param: any) {
    return http.post("/Maintenance/confirmOrder", param);
}

// 维修完成
export function repairOrderDetails(param: any) {
    return http.get("/Maintenance/repairOrderDetails", param);
}

// 报价接口
export function quotation(param: any) {
    return http.post("/Maintenance/quotation", param);
}

// 维修完成
export function RepairCompleted(param: any) {
    return http.post("/Maintenance/RepairCompleted", param);
}

// 查询所有可派遣人员
export function getPersonnelList(param: any) {
    return http.post("/Maintenance/getPersonnelList", param);
}
