import { http } from "@/utils/request"

// 查询所有设备异常故障信息列表
export function selectDeviceExceptionList(param: any) {
    return http.get("/deviceException/selectDeviceExceptionList", param)
}