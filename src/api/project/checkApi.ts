import { http } from "@/utils/request";

export function deviceBindSelectAll(param: any) {
    return http.get("/deviceBind/selectAll", param);
}

export function deviceBindInsert(project: any) {
    return http.post("/deviceBind/insert", project);
}
export function deviceBindDelete(ids: any) {
    return http.post("/deviceBind/delete", undefined, { idList: ids });
}

export function deviceBindUpdate(myParam: any) {
    return http.post("/deviceBind/update", myParam);
}

