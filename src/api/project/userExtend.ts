import { http } from "@/utils/request";

// 获取当前城市运营中心推荐关系数据
export function recommendSelectRecommendByOpertId(param: any) {
    return http.get("/recommend/selectRecommendByOpertId", param)
}