import { http } from "@/utils/request";

export function backSelectRent(param: any) {
    return http.get("/back/selectRent", param);
}
export function backSelectOrder(param: any) {
    return http.get("/back/selectOrder", param);
}
export function deviceRentUnbindConfirm(param: any) {
    return http.get("/deviceRent/unbindConfirm", param);
}

export function backSelectIncome(param: any) {
    return http.get("/back/selectIncome", param);
}

export function backSelectRentHeart(param: any) {
    return http.get("/back/selectRentHeart", param);
}
export function backSelectRentHeartStore(param: any) {
    return http.get("/back/selectRentHeartStore", param);
}
export function backSelectRentHeartStoreAll(param: any) {
    return http.get("/back/selectRentHeartStoreAll", param);
}
export function backSelectOrderHeart(param: any) {
    return http.get("/back/selectOrderHeart", param);
}
export function deviceRentUnbindConfirmHeart(param: any) {
    return http.get("/deviceRent/unbindConfirmHeart", param);
}

export function backSelectIncomeHeart(param: any) {
    return http.get("/back/selectIncomeHeart", param);
}
export function backSelectHeart(param: any) {
    return http.get("/back/selectHeart", param);
}

// 获取运营中心和服务中心总数
export function getCentres(param: any) {
    return http.get("/back/getCentres", param);
}

// 获取设备信息总数
export function getAllDeviceStatus(param: any) {
    return http.get("/back/getAllDeviceStatusByoperat", param);
}

// 获取用户总数
export function getNurseSum(param: any) {
    return http.get("/back/getNurseSum", param);
}

// 获取各服务站点各月份收益情况
export function getServerEarningsByYear(param: any) {
    return http.get("/earnings/getServerEarningsByYear", param);
}

// 获取服务中心用户数
export function getServerUser(param: any) {
    return http.get("/back/getServerUser", param);
}

// 获取服务中心设备数
export function getServerDevice(param: any) {
    return http.get("/back/getServerDevice", param);
}

// 获取服务中心收益
export function getServerEarnings(param: any) {
    return http.get("/back/getServerEarnings", param);
}

// 当日新增设备数
export function newlyIncreasedDevice(param: any) {
    return http.get("/nurseDevice/newlyIncreasedDevice", param);
}

// 当日新增用户数
export function newlyIncreasedUser(param: any) {
    return http.get("/nurseUser/newlyIncreasedUser", param);
}

// 当日收益
export function getCurrentEarnings(param: any) {
    return http.get("/earnings/getCurrentEarnings", param);
}

// 昨日收益
export function getYesterdayEarnings(param: any) {
    return http.get("/earnings/getYesterdayEarnings", param);
}

// 本月收益
export function getCurrentMonthEarnings(param: any) {
    return http.get("/earnings/getCurrentMonthEarnings", param);
}

// 上月收益
export function getLastMonthEarnings(param: any) {
    return http.get("/earnings/getLastMonthEarnings", param);
}