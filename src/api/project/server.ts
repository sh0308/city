import { http } from "@/utils/request";

// 查询当前运营中心下所有服务站点
export function nurseServerSelectAll(param: any) {
    return http.get("/nurseServer/selectNurseByUserId", param);
}

// 插入一条服务商
export function nurseServerInsert(param: any) {
    return http.post("/nurseServer/insert", param);
}

// 修改指定服务商
export function nurseServerUpdate(param: any) {
    return http.post("/nurseServer/update", param);
}