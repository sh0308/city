import { http } from "@/utils/request";

export function sysResourceSelectAll(currentPage?: Number, pageSize?: Number) {
    return http.get("/sysResource/selectAll", {
        currentPage: currentPage,
        size: pageSize,
    });
}
export function sysResourceGetResourceByRoleId(roleId: Number) {
    return http.get("/sysResource/getResourceByRoleId", {
        roleId: roleId
    });
}

export function sysResourceInsert(project: any) {
    return http.post("/sysResource/insert", project);
}
export function sysResourceDelete(ids: any) {
    return http.post("/sysResource/delete", undefined, { idList: ids });
}

export function sysResourceUpdate(myParam: any) {
    return http.post("/sysResource/update", myParam);
}

