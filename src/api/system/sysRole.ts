import { http } from "@/utils/request";

export function sysRoleSelectAll(currentPage?: Number, pageSize?: Number) {
    return http.get("/sysRole/selectAll", {
        currentPage: currentPage,
        size: pageSize,
    });
}

export function sysRoleInsert(project: any) {
    return http.post("/sysRole/insert", project);
}
export function sysRoleDelete(ids: any) {
    return http.post("/sysRole/delete", undefined, { idList: ids });
}

export function sysRoleUpdate(myParam: any) {
    return http.post("/sysRole/update", myParam);
}
export function sysRoleBindRoleByResourceId(roleId: any, ids: any) {
    return http.post("/sysRole/bindRoleByResourceId?roleId=" + roleId, ids);
}

