import { http } from "@/utils/request";

export function sysUserSelectAll(currentPage: Number, pageSize: Number) {
    return http.get("/sysUser/selectAll", {
        currentPage: currentPage,
        size: pageSize,
    });
}


export function getServiceUserId(serviceUserId: Number) {
    return http.get("/sysUser/selectserviceUserById", {
        nodeid: serviceUserId
    });
}

export function sysUserInsert(project: any) {
    return http.post("/sysUser/insert", project);
}
export function serviceUserInsert(userName: any,nodeName:any,lastIp:any,token:any,createdBy:any,serviceUserId: any) {
    return http.post("/sysUser/serviceUserInsert",{userName:userName,nodeName:nodeName,lastIp:lastIp,token:token,createdBy:createdBy,nodeId:serviceUserId} );
}
export function sysUserDelete(ids: any) {
    return http.post("/sysUser/delete", undefined, { idList: ids });
}


export function serviceUserUpdate(userName: any,nodeName:any,lastIp:any,token:any,createdBy:any) {
    return http.post("/sysUser/serviceUserUpdate", {userName:userName,nodeName:nodeName,lastIp:lastIp,token:token,createdBy:createdBy});
}
export function sysUserUpdate(myParam: any) {
    return http.post("/sysUser/update", myParam);
}

export function sysUserBindByRoleId(userId: any, roleId: any) {
    return http.post("/sysUser/bindByRoleId", undefined, {
        userId: userId,
        roleId: roleId
    });
}


export function Login(userPhone: any, userPassword: any) {
    return http.post("/backstage/login", {
        userPhone: userPhone,
        userPassword: userPassword,
    });
}

