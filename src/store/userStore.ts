import { defineStore } from "pinia";
import { ref } from "vue";

export const useUserStore = defineStore("userStore", () => {
    // ===========  变量  =============
    let userInfo: any = ref({
        userId: 1,
        roleId: 1,
        userName: "admin",
        userPassword: "",
        cardId: 0,
        nodeId: 0,
        centerId: 0,
        token: "",
        lastIp: null,
        createdBy: null,
        createdTime: null,
        updatedBy: null,
        updatedTime: null,
        deletedId: 0
    });
    let dcLanguage = ref("English");
    // ===========  函数  =============
    // ===========  返回结果  =============
    return { userInfo, dcLanguage }
}) 