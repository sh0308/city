import { Itab } from '@/type';
import { defineStore } from 'pinia';
import { ref } from 'vue';
import { useRouter } from 'vue-router';

export const useTabsStore = defineStore('tabs', () => {
    // ===========  变量  =============
    // 用于存储tab的集合
    const tabsList = ref<Array<Itab>>([]);
    // 右击的path
    let contextMenuTabPath = ref<string>("");
    // 当前点击的path
    let currentMenuTabPath = ref<string>("");

    const router = useRouter();

    // ===========  函数  =============
    // 往tab集合里面添加元素
    function addTab(tab: Itab) {
        // 判断添加的tab是否已存在
        console.log('tab :>> ', tab);
        const isSome: boolean = tabsList.value.some((item) => item.path == tab.path);
        if (tab.path !== "/home" && tab.path !== "/login" && tab.showFlag != false) {
            // 如果不存在，则添加
            if (!isSome) {
                tabsList.value.push(tab);
            }
        }

    }
    // 根据路由路径，往tab集合里面删除元素
    function closeCurrentTab(tabPath: string) {
        const index = tabsList.value.findIndex((item) => item.path == tabPath);
        console.log('index :>> ', index);
        tabsList.value.splice(index, 1);
    }

    // 关闭tab标签
    function closeOtherTabs(par?: string) {
        if (par === "left") {
            const index = tabsList.value.findIndex(
                (item) => item.path == contextMenuTabPath.value
            );
            tabsList.value.splice(0, index); // 删除第一个到当前的tab
        } else if (par === "right") {
            const index = tabsList.value.findIndex(
                (item) => item.path == contextMenuTabPath.value
            );
            tabsList.value.splice(index + 1); // 删除index处的数据到最后
        } else if (par === "other") {
            tabsList.value = tabsList.value.filter(
                (item) => item.path == contextMenuTabPath.value
            );
        } else {
            tabsList.value = [];
            contextMenuTabPath.value = "/home"
        }
        if (currentMenuTabPath.value != "/home" && tabsList.value.findIndex((item) => item.path == currentMenuTabPath.value) === -1) {
            router.push({ path: contextMenuTabPath.value });
        }
    }
    return { tabsList, contextMenuTabPath, currentMenuTabPath, addTab, closeCurrentTab, closeOtherTabs }
})