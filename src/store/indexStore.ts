import { defineStore } from "pinia";
import { ref } from "vue";

export const useIndexStore = defineStore('indexStore', () => {
    let collapseFlag = ref(false);
    return { collapseFlag }
})