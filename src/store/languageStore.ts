import { defineStore } from "pinia";

import zhCn from "element-plus/lib/locale/lang/zh-cn";
import en from "element-plus/lib/locale/lang/en";
import { computed, ref } from "vue";
export const useLanguageStore = defineStore('languageStore', () => {
    const language = ref('zh-cn')
    const locale = computed(() => (language.value === 'zh-cn' ? zhCn : en))
    return {
        language,
        locale
    }
}); 