import { App } from "vue";
import MySvgIcon from "./src/index.vue";

export default {
    install(app: App) {
        app.component("my-svg-icon", MySvgIcon)
    }
}