import { App } from "vue"
import MyDynamicNum from "./src/index.vue"
export default {
    install(app: App) {
        app.component("my-dynamicNum", MyDynamicNum)
    }
}