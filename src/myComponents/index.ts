import { App } from "vue";
//  ==定义全局组件==
// 1. 引入需要的组件
import myContainer from "./myContainer";
import myAside from "./myAside";
import myHeader from "./myHeader";
import HeaderBar from "./HeaderBar";
import myForm from "./myForm";
import myTable from "./myTable";
import myDynamicNum from "./myDynamicNum";
import cImage from "./cImage";
import mySvgIcon from "./mySvgIcon";
import myTitle from "./myTitle";
import myImgList from "./myImgList";
import myPagination from "./myPagination";

// 2. 把组件加入组件数组里面
const components = [myContainer, myAside, myHeader, HeaderBar, myForm, myTable, myDynamicNum, cImage, mySvgIcon, myTitle, myImgList, myPagination];

// 3. 导出并注册列表里面的组件
export default {
  install(app: App) {
    components.map((item) => {
      app.use(item);
    });
  },
};
