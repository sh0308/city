import { App } from "vue"
import CImage from "./src/index.vue"
export default {
    install(app: App) {
        app.component("c-image", CImage)
    }
}