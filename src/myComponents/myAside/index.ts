import { App } from "vue";
import MyAside from "./src/index.vue";
// import MyMenu from "./src/menu"
export default {
  install(app: App) {
    app.component("my-aside", MyAside);
    // app.component("my-menu", MyMenu);
  },
};
