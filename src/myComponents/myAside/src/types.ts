export interface MenuData {
  // 导航栏的图标
  icon?: string;
  // tsx导航栏最终图标
  i?: any;
  // 导航的名字
  name: string;
  // 导航的标识
  index: string;
  // 导航的子菜单
  children?: MenuData[];
}
