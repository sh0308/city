import { App } from "vue"
import MyContainer from "./src/index.vue"
export default {
    install(app: App) {
        app.component("my-container", MyContainer)
    }
}