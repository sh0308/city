import { App } from "vue"
import myImgList from "./src/index.vue"
export default {
    install(app: App) {
        app.component("my-img-list", myImgList)
    }
}