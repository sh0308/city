import { App } from "vue"
import MyTable from "./src/index.vue"
export default {
    install(app: App) {
        app.component("my-table", MyTable)
        app.component("my-jiaoyijilu", MyTable)
    }
}