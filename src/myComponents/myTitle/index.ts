import { App } from "vue"
import myTitle from "./src/index.vue"
export default {
    install(app: App) {
        app.component("my-title", myTitle)
    }
}