export type Itab = {
    path: string;
    title: string;
    showFlag: boolean;
}