import { useMessageStore } from "@/store/message";
import { ref } from "vue";
import { isEmpty } from ".";
class Message {
    // 创建一个类实例
    private static instance: Message;

    public static connected = false;
    public static connecting = false;
    public static msg: any;
    public socketTask: any;

    // 单例模式获取实例的方法
    public static getInstance(): Message {
        if (!this.instance) {
            this.instance = new Message();
        }
        return this.instance;
    }

    public stopConnect() {
        Message.connected = false;
        // Message.connecting = true;
    }

    public connect(userId: any) {
        // 获取用户id，用于连接
        if (Message.connected && !Message.connecting) {
            return false;
        }
        // 判断是否正在连接
        Message.connecting = false;
        Message.connected = true;
        let webSocketUrl = "";
        let hh = window.location.href;
        console.log('window.location.href :>> ', window.location.href);
        if (hh.indexOf('https') > -1) {
            webSocketUrl = "wss://" + "www.pmpfworld.com:443/chat" + "/messageSocket/" + userId
        } else {
            webSocketUrl = "ws://" + "8.135.99.134:8082" + "/messageSocket/" + userId
        }
        // 连接
        Message.getInstance().socketTask = new WebSocket(
            webSocketUrl
        );
        console.log(Message.getInstance().socketTask);
        // 打开连接
        Message.getInstance().socketTask.onmessage = function (msg: any) {
            console.log("接受消息");
            console.log('res :>> ', msg);
            let data = JSON.parse(msg.data);
            const messageStore = useMessageStore();
            messageStore.messageInfoListPush(data.data);
        };
        // 打开连接
        Message.getInstance().socketTask.onOpen((res: any) => {
            Message.connecting = false;
            Message.connected = true;
            console.log("打开连接");
        });
        // 错误执行
        Message.getInstance().socketTask.onError((err: any) => {
            Message.connecting = false;
            Message.connected = false;
            console.log("错误");
            console.log("onError", err);
        });
        // 接收消息
        // Message.getInstance().socketTask.onmessage = function (msg: any) {
        //     console.log("接受消息");
        //     console.log('res :>> ', msg);
        // };
        // 关闭连接
        Message.getInstance().socketTask.onClose((res: any) => {
            Message.connected = false;
            Message.getInstance().socketTask = false;
            Message.msg = false;
            console.log("onClose", res);
        });
        console.log("task", Message.getInstance().socketTask);
    }
}

export const connect = (userId: any) => {
    if (isEmpty(userId)) {
        return;
    }
    const message = Message.getInstance();
    message.connect(userId);
}

export const getSocket = () => {
    const message = Message.getInstance();
    return message.socketTask;
}

export const newSocket = () => {
    const message = new Message();
    return message.socketTask;
}

export const stopConnect = () => {
    const message = Message.getInstance();
    message.stopConnect();
}