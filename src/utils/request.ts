import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { ElMessage } from "element-plus";
import "element-plus/es/components/message/style/css";
import { getLocalStorage } from ".";
import { ElLoading } from 'element-plus';
import { useRouter } from "vue-router";
import NProgress from 'nprogress';
/**
 *url?: string;
  method?: Method | string;
  baseURL?: string;
  transformRequest?: AxiosRequestTransformer | AxiosRequestTransformer[];
  transformResponse?: AxiosResponseTransformer | AxiosResponseTransformer[];
  headers?: AxiosRequestHeaders;
  params?: any;
  paramsSerializer?: (params: any) => string;
  data?: D;
  timeout?: number;
  timeoutErrorMessage?: string;
 */
class MyHttp {
  constructor() {
    this.httpInterceptorsRequest();
    this.httpInterceptorsResponse();
  }

  // public static baseIP = "192.168.1.22";
  public static baseIP = "8.135.99.134";

  private static request: AxiosInstance = axios.create({
    baseURL: "http://" + MyHttp.baseIP + ":8099",
    timeout: 600000,
    headers: { "Content-Type": "application/json" },
    timeoutErrorMessage: "超时了~",
  });

  // 请求拦截
  private httpInterceptorsRequest(): void {
    MyHttp.request.interceptors.request.use(
      (config: any) => {
        // ElLoading.service({
        //   fullscreen: true,
        //   text: "正在加载中，请稍后",
        // })
        NProgress.start();
        //   config.url = "www.baidu.com";
        const token = getLocalStorage("token");
        if (!token) {
          config.headers = {
            ...config.headers,
            token: getLocalStorage("token"),
            userId: getLocalStorage("userId"),
          };
        }
        return config;
      },
      (error) => {
        ElMessage.error("服务器错误");
        console.log(error);
        return Promise.reject(error);
      }
    );
  }

  // 响应拦截
  private httpInterceptorsResponse(): void {
    MyHttp.request.interceptors.response.use(
      (res) => {
        NProgress.done();
        // ElLoading.service().close()
        if (res.data.code === 402) {
          ElMessage.info("请先登录!");
          let currentUrl = window.location.href;
          let mainUrl = currentUrl.split("#")[0] + "#/login";
          location.href = mainUrl;
        }
        if (res.data.code === 403) {
          ElMessage.info("没有授权，请联系超级管理员！");
        } else if (res.data.code === 500) {
          ElMessage.error(res.data.msg);
        }
        return res.data;
      },
      (error) => {
        NProgress.done();
        ElMessage.error("服务器错误");
        // ElLoading.service().close()
        return Promise.reject(error);
      }
    );
  }

  // 通用请求工具函数
  public request<T>(method: string, url: string, param?: AxiosRequestConfig, data?: any, headers?: any): Promise<T> {
    // 单独处理自定义请求/响应回掉
    return new Promise((resolve, reject) => {
      MyHttp.request
        .request({
          method,
          url,
          params: param,
          data,
          headers: {
            ...headers,
            token: getLocalStorage("token"),
            userId: getLocalStorage("userId"),
          }
        })
        .then((response: any) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  // 通用请求工具函数
  public uploadFile<T>(url: string, data?: any, headers?: any): Promise<T> {
    // 单独处理自定义请求/响应回掉
    return new Promise((resolve, reject) => {
      MyHttp.request
        .request({
          method: "post",
          url,
          data,
          headers: {
            "Content-Type": "multipart/form-data",
            ...headers,
            token: getLocalStorage("token"),
            userId: getLocalStorage("userId"),
          },
          onUploadProgress: (progressEvent) => {
            console.log('progressEvent :>> ', progressEvent);
          },
        })
        .then((response: any) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  // 单独抽离的post工具函数
  public post(url: string, params?: any, data?: any, header?: any): Promise<any> {
    return this.request("post", url, data, params, header);
  }

  // 单独抽离的get工具函数
  public get(url: string, params?: any): Promise<any> {
    return this.request("get", url, params);
  }
  // 单独抽离的get工具函数
  public upload(url: string, params?: any): Promise<any> {
    return this.uploadFile(url, params);
  }
}

export const http = new MyHttp();

export const getHttp = (url: string, params?: any) => {
  return http.request("get", url, params);
};

export const postHttp = (url: string, params?: any, data?: any) => {
  return http.request("post", url, params, data);
};


export const getIp = () => {
  return MyHttp.baseIP;
}