import { ElMessage, ElMessageBox, } from 'element-plus';
import { markRaw } from 'vue'
import { WarningFilled } from '@element-plus/icons-vue';
import { useRouter } from "vue-router";

export const setLocalStorage = (key: string, value: any) => {
    localStorage.setItem(key, JSON.stringify(value));
}

export const getLocalStorage = (key: string) => {
    if (isEmpty(localStorage.getItem(key))) return;
    return localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key) as string) : {};
}

export const setSessionStorage = (key: string, value: any) => {
    sessionStorage.setItem(key, JSON.stringify(value));
}

export const getSessionStorage = (key: string) => {
    return sessionStorage.getItem(key) ? JSON.parse(localStorage.getItem(key) as string) : {};
}

export const popupMessage = (title: string = "操作", desc: string = "确定执行该操作吗？", icon: object = WarningFilled, cancelText: string = "取消", confirmText: string = "确认"): Promise<any> => {
    return ElMessageBox.confirm(desc, title, {
        confirmButtonText: confirmText,
        cancelButtonText: cancelText,
        distinguishCancelAndClose: true,  // 开始判断用户点击的位置是否是关闭按钮和遮罩
        type: "warning",
        draggable: true,   // 可拖动
        icon: markRaw(icon)  // 自定义图标
    });
}

export const successMessage = (content: string = "操作成功") => {
    ElMessage({
        showClose: true,
        message: content,
        type: 'success',
    })
}

export const warningMessage = (content: string = "操作警告") => {
    ElMessage({
        showClose: true,
        message: content,
        type: 'warning',
    })
}

export const infoMessage = (content: string = "操作信息") => {
    ElMessage({
        showClose: true,
        message: content,
        type: 'info',
    })
}

export const errorMessage = (content: string = "操作错误") => {
    ElMessage({
        showClose: true,
        message: content,
        type: 'error',
    })
}

/**
 * @param {String} param 根据传入值判断是否为空（无效）
 * @return {String} 判断值状态 空（无效）返回true
 */
export const isEmpty = (param: any): boolean => {
    if (Object.prototype.toString.call(param) === "[object Object]") {
        // 对象
        for (const name in param) {
            if ({}.hasOwnProperty.call(param, name)) {
                return false;
            }
        }
        return true;
    } else if (Object.prototype.toString.call(param) === "[object Array]") {
        // 数组
        return !param.length;
    } else if (
        param === undefined ||
        param === "undefined" ||
        param === null ||
        param === "null"
    ) {
        // 空值
        return true;
    } else if (param === true || param === false) {
        // 布尔值
        return false;
    }

    return !param;
};

export const objIsEmpty = (paramObj: any) => {
    let dataFlag = true;
    Object.keys(paramObj).forEach((item) => {
        if (isEmpty(paramObj[item])) {
            console.log("itemEmpty :>> ", item);
            dataFlag = false;
            return dataFlag;
        }
    });
    return dataFlag;
}


// 格式化日期
export const formatTime = (date?: any, format?: string): string => {
    // 无格式符串
    format = format ? format : 'yyyy-MM-dd HH:mm:ss';
    // 无日期
    if (isEmpty(date)) {
        return "";
        date = new Date();
    }
    // 日期类型
    if (Object.prototype.toString.call(date) !== "[object Date]") {
        // console.log('日期类型 :>> ', date);
        // 2016-1-1 -> 2016/1/1   /g 代表全局所有的 - 都替换
        date = new Date(date.replace(/-/g, "/"));
    }
    const o: any = {
        "y+": date.getFullYear(),  //年份
        "M+": date.getMonth() + 1, //月份
        "d+": date.getDate(), //日
        "h+": date.getHours() % 12 == 0 ? 12 : date.getHours() % 12, //12小时制小时
        "H+": date.getHours(), //24小时制小时
        "m+": date.getMinutes(), //分
        "s+": date.getSeconds(), //秒
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度
    }

    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length))
    }

    for (const k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(
                RegExp.$1,
                RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length)
            );
        }
    }

    return format;
}


export const formatTimeWeek = (dateStr: any) => {
    // 日期类型
    if (Object.prototype.toString.call(dateStr) !== "[object Date]") {
        // console.log('日期类型 :>> ', dateStr);
        // 2016-1-1 -> 2016/1/1   /g 代表全局所有的 - 都替换
        dateStr = new Date(dateStr.replace(/-/g, "/"));
    } else {
        dateStr = new Date();
    }
    let weekArrayList = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六',]
    let index = dateStr.getDay()
    return weekArrayList[index];
}

export const getTaskWeekList = (taskStr: any) => {
    let weekList = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"]
    let resultList = [];
    for (let i = 0; i < taskStr.length; i++) {
        if (taskStr[i] == 1) {
            resultList.push(weekList[i]);
        }

    }
    return resultList;
}

export const getTaskWeekIndex = (taskList: Array<any>) => {
    let weekList = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"]
    let resultListIndex = "";
    for (let i = 0; i < weekList.length; i++) {
        let forFlag = true;
        for (let j of taskList) {
            if (weekList[i] == j) {
                resultListIndex = resultListIndex + "" + "1"
                forFlag = false;
            }
        }
        if (forFlag) {
            resultListIndex = resultListIndex + "" + "0"
        }
    }
    return resultListIndex;
};

export const getCommandStr = (commandId: any) => {
    let commandIndexList = [
        {
            commandId: 4,
            label: "起背"
        }, {
            commandId: 5,
            label: "落背"
        }, {
            commandId: 6,
            label: "左翻"
        }, {
            commandId: 7,
            label: "右翻"
        }, {
            commandId: 8,
            label: "抬脚"
        }, {
            commandId: 9,
            label: "落脚"
        }, {
            commandId: 10,
            label: "坐起"
        }, {
            commandId: 11,
            label: "平躺"
        }, {
            commandId: 12,
            label: "头部按摩"
        }, {
            commandId: 13,
            label: "背部按摩"
        }, {
            commandId: 14,
            label: "腿部按摩"
        }, {
            commandId: 34,
            label: "播放音乐"
        }, {
            commandId: 22,
            label: "助眠"
        }, {
            commandId: 23,
            label: "唤醒"
        }, {
            commandId: 18,
            label: "除臭"
        }, {
            commandId: 20,
            label: "烘干"
        }, {
            commandId: 17,
            label: "便盆清理"
        }, {
            commandId: 99,
            label: "熄屏"
        }
    ]
    let currentCommandLabel = commandIndexList.filter(i => {
        return i.commandId == commandId
    })
    // console.log('currentCommandLabel :>> ', currentCommandLabel);
    // let commandList = [
    //     {
    //         type: "radio",
    //         label: "起背"
    //     }, {
    //         type: "radio",
    //         label: "落背"
    //     }, {
    //         type: "radio",
    //         label: "左翻"
    //     }, {
    //         type: "radio",
    //         label: "右翻"
    //     }, {
    //         type: "radio",
    //         label: "抬脚"
    //     }, {
    //         type: "radio",
    //         label: "落脚"
    //     }, {
    //         type: "radio",
    //         label: "坐起"
    //     }, {
    //         type: "radio",
    //         label: "平躺"
    //     }, {
    //         type: "radio",
    //         label: "头部按摩"
    //     }, {
    //         type: "radio",
    //         label: "背部按摩"
    //     }, {
    //         type: "radio",
    //         label: "腿部按摩"
    //     }, {
    //         type: "radio",
    //         label: "播放音乐"
    //     }, {
    //         type: "radio",
    //         label: "助眠"
    //     }, {
    //         type: "radio",
    //         label: "唤醒"
    //     }, {
    //         type: "radio",
    //         label: "除臭"
    //     }, {
    //         type: "radio",
    //         label: "便盆清理"
    //     }, {
    //         type: "radio",
    //         label: "熄屏"
    //     }
    // ]

    return currentCommandLabel[0].label;
};

export const getCommandIndex = (commandStr: any) => {
    let commandIndexList = [
        {
            commandId: 4,
            label: "起背"
        }, {
            commandId: 5,
            label: "落背"
        }, {
            commandId: 6,
            label: "左翻"
        }, {
            commandId: 7,
            label: "右翻"
        }, {
            commandId: 8,
            label: "抬脚"
        }, {
            commandId: 9,
            label: "落脚"
        }, {
            commandId: 10,
            label: "坐起"
        }, {
            commandId: 11,
            label: "平躺"
        }, {
            commandId: 12,
            label: "头部按摩"
        }, {
            commandId: 13,
            label: "背部按摩"
        }, {
            commandId: 14,
            label: "腿部按摩"
        }, {
            commandId: 34,
            label: "播放音乐"
        }, {
            commandId: 22,
            label: "助眠"
        }, {
            commandId: 23,
            label: "唤醒"
        }, {
            commandId: 18,
            label: "除臭"
        }, {
            commandId: 20,
            label: "烘干"
        }, {
            commandId: 17,
            label: "便盆清理"
        }, {
            commandId: 99,
            label: "熄屏"
        }
    ]
    let currentCommandLabel = commandIndexList.filter(i => {
        return i.label == commandStr
    })
    console.log('currentCommandLabel :>> ', currentCommandLabel);
    return currentCommandLabel[0].commandId;
};


declare const window: any;
export function loadBMap(ak: any) {
    return new Promise(function (resolve, reject) {
        let BMap = window.BMap;
        if (typeof BMap !== 'undefined') {
            resolve(BMap)
            return true
        }
        window.onBMapCallback = function () {
            resolve(BMap)
        }
        let script = document.createElement('script')
        script.type = 'text/javascript'
        script.src = 'http://api.map.baidu.com/api?v=2.0&ak=' + ak + '&__ec_v__=20190126&callback=onBMapCallback'
        script.onerror = reject
        document.head.appendChild(script)
    })
}


const router = useRouter();
export const goPage = (path: string) => {
    router.push({ path: path });
}



/**
 * 计算两个日期之间的天数
 *  date1  开始日期 yyyy-MM-dd
 *  date2  结束日期 yyyy-MM-dd
 *  如果日期相同 返回一天 开始日期大于结束日期，返回0
 * date1为空时，返回距离当前的时间
 */
export const getDaysBetween = (date1: string, date2: string) => {

    if (isEmpty(date1)) {
        let startDateTimestamp = Date.now();

        let endDate;
        if (Object.prototype.toString.call(date1) !== "[object Date]") {
            endDate = new Date(date2.replace(/-/g, "/"));
        }
        if (endDate == null) {
            return 0;
        }
        let endDateTimestamp = endDate.getTime();
        if (startDateTimestamp > endDateTimestamp) {
            return 0;
        }
        if (startDateTimestamp == endDateTimestamp) {
            return 1;
        }
        let days = ((endDateTimestamp - startDateTimestamp) / (1 * 24 * 60 * 60 * 1000)).toFixed(0);
        return days;
    }
    if (isEmpty(date2)) {
        return 0;
    }

    let startDate;
    let endDate;
    if (Object.prototype.toString.call(date1) !== "[object Date]") {
        startDate = new Date(date1.replace(/-/g, "/"));
    }
    if (Object.prototype.toString.call(date1) !== "[object Date]") {
        endDate = new Date(date2.replace(/-/g, "/"));
    }

    if (startDate == null) {
        return 0;
    }
    if (endDate == null) {
        return 0;
    }
    let startDateTimestamp = startDate.getTime();
    let endDateTimestamp = endDate.getTime();
    if (startDateTimestamp > endDateTimestamp) {
        return 0;
    }
    if (startDateTimestamp == endDateTimestamp) {
        return 1;
    }
    let days = (endDateTimestamp - startDateTimestamp) / (1 * 24 * 60 * 60 * 1000);
    return days;
}

/**
 * 计算两个日期之间相差的月份
 *  date1  开始日期 yyyy-MM-dd
 *  date2  结束日期 yyyy-MM-dd
 *  如果日期相同 返回一天 开始日期大于结束日期，返回0
 * date1为空时，返回距离当前的时间
 */
export const getMonthBetween = (startTimeStr: string, endTimeStr: string) => {
    let startTime = new Date(startTimeStr);
    let endTime = new Date(endTimeStr);
    let date2Mon;
    let startDate = startTime.getDate() + startTime.getHours() / 24 + startTime.getMinutes() / 24 / 60;
    let endDate = endTime.getDate() + endTime.getHours() / 24 + endTime.getMinutes() / 24 / 60;
    if (endDate >= startDate) {
        date2Mon = 0;
    } else {
        date2Mon = -1;
    }
    return (endTime.getFullYear() - startTime.getFullYear()) * 12 + endTime.getMonth() - startTime.getMonth() + date2Mon;
}


export const monitorZoom = () => {
    let ratio = 0,
        screen = window.screen,
        ua = navigator.userAgent.toLowerCase();
    if (window.devicePixelRatio !== undefined) {
        ratio = window.devicePixelRatio;
    } else if (~ua.indexOf("msie")) {
        if (screen.deviceXDPI && screen.logicalXDPI) {
            ratio = screen.deviceXDPI / screen.logicalXDPI;
        }
    } else if (
        window.outerWidth !== undefined &&
        window.innerWidth !== undefined
    ) {
        ratio = window.outerWidth / window.innerWidth;
    }
    if (ratio) {
        ratio = Math.round(ratio * 100);
    }
    return ratio;
};