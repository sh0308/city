export const towNumToPercent = (num1: number, num2: number) => {
    if (Number(num2) === 0) {
        return num1 * 100;
    }
    return Math.floor((num1 - num2) / num2 * 100 * 100) / 100;
}
