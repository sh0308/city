import { title } from "process";
import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";

// 1. 新建routers列表
const routerList: RouteRecordRaw[] = [
    {
        path: "/",
        name: "index",
        redirect: "/home",
        component: () => import("@/views/index/index.vue"),
        children: [
            {
                path: "/home",
                name: "StoreHome",
                component: () => import("@/views/index/home/index.vue"),
                meta: {
                    title: "首页"
                }
            },
            {
                path: "/device",
                name: "/storeDevice",
                component: () => import("@/views/index/device/index.vue"),
                meta: {
                    title: "设备列表"
                }
            },
            {
                path: "/record",
                name: "Record",
                component: () => import("@/views/index/record/index.vue"),
                meta: {
                    title: "设备申请记录"
                }
            },
            {
                path: "/record/about",
                name: "RecordAbout",
                component: () => import("@/views/index/record/about.vue"),
                meta: {
                    title: "我的设备申请"
                }
            },
            {
                path: "/transfer",
                name: "Transfer",
                component: () => import("@/views/index/transfer/index.vue"),
                meta: {
                    title: "设备调拨记录"
                }
            },
            {
                path: "/exception",
                name: "Exception",
                component: () => import("@/views/index/exception/index.vue"),
                meta: {
                    title: "异常信息"
                }
            },
            {
                path: "/lock",
                name: "Lock",
                component: () => import("@/views/index/lock/index.vue"),
                meta: {
                    title: "锁机信息"
                }
            },
            {
                path: "/lease",
                name: "Lease",
                component: () => import("@/views/index/lease/index.vue"),
                meta: {
                    title: "租赁记录"
                }
            },
            {
                path: "/leaseApply",
                name: "LeaseApply",
                component: () => import("@/views/index/lease/apply.vue"),
                meta: {
                    title: "中心退租申请",
                }
            },
            {
                path: "/selfLeaseApply",
                name: "SelfleaseApply",
                component: () => import("@/views/index/lease/operatApply.vue"),
                meta: {
                    title: "运营中心退租申请",
                }
            },
            {
                path: "/signGoods",
                name: "SignGoods",
                component: () => import("@/views/index/lease/signGoods.vue"),
                meta: {
                    title: "运营中心退租申请",
                }
            },
            {
                path: "/",
                name: "Lease",
                component: () => import("@/views/index/lease/index.vue"),
                meta: {
                    title: "租赁记录"
                }
            },
            {
                path: "/serviceCenter",
                name: "ServiceCenter",
                component: () => import("@/views/index/serviceCenter/index.vue"),
                meta: {
                    title: "服务中心管理"
                }
            },
            {
                path: "/income",
                name: "StoreIncome",
                component: () => import("@/views/index/income/index.vue"),
                meta: {
                    title: "收益明细"
                }
            },
            {
                path: "/serviceIncome",
                name: "ServiceIncome",
                component: () => import("@/views/index/serviceIncome/index.vue"),
                meta: {
                    title: "服务中心收益"
                }
            },
            {
                path: "/userRevenue",
                name: "UserRevenue",
                component: () => import("@/views/index/userRevenue/index.vue"),
                meta: {
                    title: "用户收益"
                }
            },
            {
                path: "/lesseeList",
                name: "LesseeList",
                component: () => import("@/views/index/lesseeList/index.vue"),
                meta: {
                    title: "租户列表"
                }
            },
            // {
            //     path: "/userList",
            //     name: "UserList",
            //     component: () => import("@/views/index/store/userList/index.vue"),
            //     meta: {
            //         title: "用户列表"
            //     }
            // },
            // {
            //     path: "/store/user",
            //     name: "StoreUser",
            //     component: () => import("@/views/index/store/user/index.vue"),
            //     meta: {
            //         title: "用户"
            //     }
            // },
            {
                path: "/userLeasing",
                name: "UserLeasing",
                component: () => import("@/views/index/userLeasing/index.vue"),
                meta: {
                    title: "用户租赁"
                }
            },
            {
                path: "/userExtend",
                name: "UserExtend",
                component: () => import("@/views/index/userExtend/index.vue"),
                meta: {
                    title: "推荐列表"
                }
            },
            {
                path: "/back",
                name: "StoreBack",
                component: () => import("@/views/index/back/index.vue"),
                meta: {
                    title: "退租管理"
                }
            },
            {
                path: "/user",
                name: "User",
                component: () => import("@/views/index/user/index.vue"),
                meta: {
                    title: "用户管理",
                }
            },
            {
                path: "/user/desc",
                name: "UserDesc",
                component: () => import("@/views/index/user/desc.vue"),
                meta: {
                    title: "用户详细信息",
                    breadcrumbFlag: false  // 不在面包屑展示
                }
            },
            {
                path: "/protocol",
                name: "Protocol",
                component: () => import("@/views/index/protocol/index.vue"),
                meta: {
                    title: "协议管理"
                }
            },
            {
                path: "/desc",
                name: "ProtocolDesc",
                component: () => import("@/views/index/protocol/desc.vue"),
                meta: {
                    title: "协议详细信息",
                    breadcrumbFlag: false  // 不在面包屑展示
                }
            },
            {
                path: "/notice",
                name: "Notice",
                component: () => import("@/views/index/notice/index.vue"),
                meta: {
                    title: "通知管理"
                }
            },
            {
                path: "/notice/desc",
                name: "NoticeDesc",
                component: () => import("@/views/index/notice/desc.vue"),
                meta: {
                    title: "通知详细信息",
                    breadcrumbFlag: false  // 不在面包屑展示
                }
            },
            {
                path: "/news",
                name: "News",
                component: () => import("@/views/index/news/index.vue"),
                meta: {
                    title: "新闻管理"
                }
            },
            {
                path: "/news/desc",
                name: "NewsDesc",
                component: () => import("@/views/index/news/desc.vue"),
                meta: {
                    title: "新闻详细信息",
                    breadcrumbFlag: false  // 不在面包屑展示
                }
            },
            {
                path: "/extend",
                name: "Extend",
                component: () => import("@/views/index/extend/index.vue"),
                meta: {
                    title: "推广福利"
                }
            },
            {
                path: "/extend/desc",
                name: "ExtendDesc",
                component: () => import("@/views/index/extend/desc.vue"),
                meta: {
                    title: "推广福利详细信息",
                    breadcrumbFlag: false  // 不在面包屑展示
                }
            },
            // {
            //     path: "/image",
            //     name: "Image",
            //     component: () => import("@/views/index/image/index.vue"),
            //     meta: {
            //         title: "图片管理"
            //     }
            // },
            {
                path: "/shop/category",
                name: "ShopCategory",
                component: () => import("@/views/index/shop/category/index.vue"),
                meta: {
                    title: "商品分类"
                }
            },
            {
                path: "/shop/home",
                name: "ShopHome",
                component: () => import("@/views/index/shop/home/index.vue"),
                meta: {
                    title: "商品管理"
                }
            },
            {
                path: "/shop/home/desc",
                name: "ShopHomeDesc",
                component: () => import("@/views/index/shop/home/desc.vue"),
                meta: {
                    title: "商品详细信息",
                    breadcrumbFlag: false  // 不在面包屑展示
                }
            },
            {
                path: "/shop/search/index",
                name: "SearchIndex",
                component: () => import("@/views/index/shop/search/index.vue"),
                meta: {
                    title: "热门搜索"
                }
            },
            // {
            //     path: "/system/user",
            //     name: "SystemUser",
            //     component: () => import("@/views/system/user/index.vue"),
            //     meta: {
            //         title: "系统用户管理"
            //     }
            // },
            // {
            //     path: "/system/role",
            //     name: "SystemRole",
            //     component: () => import("@/views/system/role/index.vue"),
            //     meta: {
            //         title: "系统角色管理"
            //     }
            // },
            // {
            //     path: "/system/resource",
            //     name: "SystemResource",
            //     component: () => import("@/views/system/resource/index.vue"),
            //     meta: {
            //         title: "系统权限资源管理"
            //     }
            // },
            // {
            //     path: "/store/rent",
            //     name: "StoreRent",
            //     component: () => import("@/views/index/store/rent/index.vue"),
            //     meta: {
            //         title: "租赁信息"
            //     }
            // },
            {
                path: "/transaction",
                name: "StoreTransaction",
                component: () => import("@/views/index/transaction/index.vue"),
                meta: {
                    title: "交易记录"
                }
            },
            {
                path: "/notice",
                name: "StoreNotice",
                component: () => import("@/views/index/notice/index.vue"),
                meta: {
                    title: "系统通知"
                }
            },
            {
                path: "/logistics",
                name: "Logistics",
                component: () => import("@/views/index/logistics/index.vue"),
                meta: {
                    title: "物流查询",
                }
            },
            {
                path: "/workOrder",
                name: "WorkOrder",
                component: () => import("@/views/index/workOrder/index.vue"),
                meta: {
                    title: "报修工单",
                }
            },
            {
                path: "/report",
                name: "Report",
                component: () => import("@/views/index/report/index.vue"),
                meta: {
                    title: "设备报表",
                }
            },
            {
                path: "/repertory",
                name: "Repertory",
                component: () => import("@/views/index/repertory/index.vue"),
                meta: {
                    title: "库存管理",
                }
            }
        ]
    },
    {
        path: "/login",
        name: "Login",
        component: () => import("@/views/login/index.vue")
    },
    // {
    //     path: "/map",
    //     name: "Map",
    //     component: () => import("@/views/index/home/map.vue"),
    //     meta: {
    //         title: "地图",
    //     },
    // }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes: routerList,
});

export default router;


