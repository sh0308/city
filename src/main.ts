import { createApp } from 'vue'
import App from './App.vue'

import "@/style/index.scss" // 全局样式

// 1. 导入路由
import router from './router';
// 2. 完整引入ElementPlus和样式
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';

import { getLocalStorage, setLocalStorage } from './utils';

// 3. 
import { PiniaPluginContext, createPinia } from 'pinia';
// 4. 完整引入自定义组件库
import myComponents from '@/myComponents/index';

// 5. 引入ElementPlusIcon
import * as ElementPlusIconsVue from "@element-plus/icons-vue";

//顶部页面加载条
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
NProgress.configure({
    easing: 'ease',
    speed: 500,
    showSpinner: false,
    trickleSpeed: 200,
    minimum: 0.3
})
//路由监听
router.beforeEach((to, from, next) => {
    NProgress.start();
    next();
});
//路由跳转结束
router.afterEach(() => {
    NProgress.done()
})


// 3. 配置Pinia持久化
type Options = {
    key?: string
}
const __piniaKey__ = 'myPinia'
const piniaPlugin = (options: Options) => {
    return (context: PiniaPluginContext) => {
        const { store } = context;
        // 刷新加载时，获取data数据
        const data = getLocalStorage(`${options?.key ?? __piniaKey__}-${store.$id}`)
        // 当pinia状态值发生变化时，执行
        store.$subscribe(() => {
            setLocalStorage(`${options?.key ?? __piniaKey__}-${store.$id}`, store.$state)
        })
        return {
            ...data
        }
    }
}
const store = createPinia();
// 使用pinia插件
store.use(piniaPlugin({
    key: "pinia"
}));

// import { monitorZoom } from "@/utils/index.ts";
// const m = monitorZoom();
// if (window.screen.width * window.devicePixelRatio >= 3840) {
//     document.body.style.zoom = 100 / (Number(m) / 2); // 屏幕为 4k 时
// } else {
//     document.body.style.zoom = 100 / Number(m);
// }

let app = createApp(App);


// 1. 导入路由
app.use(router);
// 2. 完整引入ElementPlus
app.use(ElementPlus);
// 3. 配置Pinia
app.use(store);
// 4. 完整引入自定义组件库
app.use(myComponents);

// 5. 引入ElementPlusIcon
for (const name in ElementPlusIconsVue) {
    app.component(name, (ElementPlusIconsVue as any)[name]);
}
// 全部配置完最后挂载
app.mount('#app');